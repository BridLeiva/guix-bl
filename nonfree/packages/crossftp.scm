(define-module (nonfree packages crossftp)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages java)
  #:use-module (gnu packages compression))

(define-public crossftp
  (package
    (name "crossftp")
    (version "1.99.9")
    (source (origin
            (method url-fetch)
            (uri (string-append "http://crossftp.com/crossftp-all-bin-" version ".zip"))
            (sha256
             (base32
              "0k03400lzh9c12pi552x5sc1s85wh4p8bp3fmz8s938ycinm3xy8"))))
    (build-system copy-build-system)
    (arguments '(#:install-plan '(("./run_client.sh" "bin/crossftp") ("." "bin"))))
  	(native-inputs (list unzip))
  	(propagated-inputs (list openjdk17 coreutils which))
    (synopsis "CrossFTP: FTP & Amazon S3 Client")
    (description "CrossFTP: FTP & Amazon S3 Client")
    (home-page "http://crossftp.com/index.htm")
    (license non-copyleft)))

